package com.googleOAuth2.springsecuritygoogleoauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityGoogleOauth2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityGoogleOauth2Application.class, args);
	}

}
