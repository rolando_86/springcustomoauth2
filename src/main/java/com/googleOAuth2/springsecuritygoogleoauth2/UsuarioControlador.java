package com.googleOAuth2.springsecuritygoogleoauth2;

import org.modelmapper.ModelMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioControlador {

    @RequestMapping(value = "/user")
    public Object tokenGoogle(@RequestParam("NroPagina") String code) {



        Authentication algo = SecurityContextHolder.getContext().getAuthentication();


        //credentials
        Object credentials = algo.getCredentials();
        System.out.println(credentials);
        System.out.println();
        System.out.println();

        //details
        Object details = algo.getDetails();
        System.out.println(details);
        System.out.println();
        System.out.println();

        Details details1 = new ModelMapper().map(details, Details.class);
        System.out.println(details1.toString());
        System.out.println();
        System.out.println();

        //roles
        Object dato = algo.getAuthorities();
        System.out.println(dato);
        System.out.println();
        System.out.println();

        //principal
        Object principal2 = algo.getPrincipal();
        System.out.println(principal2);
        System.out.println();
        System.out.println();

        //Principal principal1 = principal;


        return "hola: "+algo.getPrincipal();
    }
}
